import { createStore, applyMiddleware, compose } from 'redux';
import thunk  from 'redux-thunk';
import rootReducer from './rootReducer';
const initialState = {};
const composeEnhancer = window.__REDUX_DEVTOOL_EXTENSION_COMPOSE__ || compose;

export default createStore(rootReducer, initialState, composeEnhancer(applyMiddleware(thunk)));