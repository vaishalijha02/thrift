import React, { Component } from 'react';
import History from './utils/history';
import {Router, Route, Switch} from 'react-router';
import Home from './components/HomeComponent/home';
import Header from './components/HeaderComponent/header';
import SignIn from './components/authComponent/signIn';
import SignUp from './components/authComponent/signUp';
import { connect } from "react-redux";
import Dashboard from './components/dashoboardComponent/dashboard';
import PersonaType from './components/personaComponent/personaType/type';
import PersonaExpense from './components/personaComponent/personaExpense/expense';


class Routes extends Component {
    render() {
        return (
            <div>
                <Router history={History}>
                <Header></Header>
                    <div>
                        <Switch>
                            <Route exact path="/" component = {Home}/>
                            <Route path = "/home" component = {Home}/>
                            <Route path = "/login" component = {SignIn} />
                            <Route path = "/signup" component = {SignUp} />
                            <Route path = "/signout" component = {Home} />
                            <Route path = "/dashboard" component = {Dashboard} />
                            <Route path = '/persona/type' component = {PersonaType} />
                            <Route path = '/persona/expense' component = {PersonaExpense} />
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({isAuthUser: state.authDetails.isAuthUser})
export default connect(mapStateToProps)(Routes);
