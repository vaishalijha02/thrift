import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import './header.css';
import { logOut } from './../../services/actions/loginAction';


class Header extends Component {
    resetAuthStatus = () => {
        this.props.logOut();
    }
    render() {
        return (
            <div className="row">
                <div className="col-12 pr-0">
                    <nav className="navbar navbar-expand-sm fixed-top bg-transparent ">
                        <Link to="/">
                            <img className="pl-3" src={require('./../../assets/logo_transparent.png')} alt="logo"></img>
                        </Link>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarToggle"> 
                            <ul className="nav navbar-nav  ml-auto">
                                <li role="presentation" className="nav-item" >
                                { this.props.isAuthUser
                                    ? <NavLink className="nav-link" to="/signout" onClick= {this.resetAuthStatus}><div className="btn btn-primary btn-sm">LOG OUT</div></NavLink>
                                    : <NavLink className="nav-link" to="/login" ><span className="btn btn-primary btn-sm">LOG IN</span></NavLink>
                                }   
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>    
        )
    }
}
const mapStateToProps = (state) => ({isAuthUser: state.authDetails.isAuthUser});
export default connect(mapStateToProps, { logOut })(Header);