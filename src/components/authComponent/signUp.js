import React, { Component } from 'react';
import './signUp.css';
import {createAccount} from './../../services/actions/signUpAction';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

class SignUp extends Component {
    userDetails = {};

    handleChange = (event) => {
        this.userDetails[event.target.name] = event.target.value;
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.createAccount(this.userDetails);
        this.userDetails = {};
    }
    render() {
        return (
            <div className="row container-height">
                <div className="home-background"></div>
                <div className="content">
                    <div className="content-text ">
                        <div className="col-xs-12 col-sm-8 ml-auto mr-auto">
                            <h3 className="pb-3">Create an account</h3>  
                            <form onSubmit={this.handleSubmit}>
                                <div className="text-left form-group">
                                    <label>Name</label>
                                    <input type="text" className={`form-control ${this.props.signupError && this.props.signupError.name ? "is-invalid" : ""}`} aria-describedby="name" placeholder="Enter name" onChange={this.handleChange} name="name"/>
                                </div>
                                <div className="text-left form-group">
                                    <label>Email address</label>
                                    <input type="email" className={`form-control ${this.props.signupError && this.props.signupError.email ? "is-invalid" : ""}`} id="sign_up_email_id" aria-describedby="emailHelp" placeholder="Enter email" onChange={this.handleChange} name="email"/>
                                    { this.props.signupError && this.props.signupError.email
                                    ? <div className="invalid-feedback">{this.props.signupError.email}</div>
                                    : <div></div>
                                    }
                                </div>
                                <div className="text-left form-group">
                                    <label>Password</label>
                                    <input type="password" className={`form-control ${this.props.signupError && this.props.signupError.password ? "is-invalid" : ""}`} id="sign_up_password" placeholder="Password" onChange={this.handleChange} password="password" name="password"/>
                                    { this.props.signupError && this.props.signupError.password
                                        ? <div className="invalid-feedback">{this.props.signupError.password}</div>
                                        : <div></div>
                                    }
                                </div>
                                <div className="text-left form-group">
                                    <label>Re-enter Password </label>
                                    <input type="password" className={`form-control ${this.props.signupError && this.props.signupError.confirmPassword ? "is-invalid" : ""}`} placeholder="Re-enter Password" onChange={this.handleChange} name="confirmPassword"/>
                                    { this.props.signupError && this.props.signupError.confirmPassword
                                        ? <div className="invalid-feedback">{this.props.signupError.confirmPassword}</div>
                                        : <div></div>
                                    }
                                </div> 
                                <div className="text-left form-group">
                                    <label>Phone Number </label>
                                    <input type="number" className={`form-control ${this.props.signupError && this.props.signupError.phone ? "is-invalid" : ""}`} placeholder="Phone Number" onChange={this.handleChange} name="phone"/>
                                    { this.props.signupError && this.props.signupError.phone
                                        ? <div className="invalid-feedback">{this.props.signupError.phone}</div>
                                        : <div></div>
                                    }
                                </div> 
                                <div className="text-left form-group">
                                    <label>User handle</label>
                                    <input type="text" className={`form-control ${this.props.signupError && this.props.signupError.handle ? "is-invalid" : ""}`} placeholder="Handle" onChange={this.handleChange} name="handle"/>
                                    { this.props.signupError && this.props.signupError.handle
                                        ? <div className="invalid-feedback">{this.props.signupError.handle}</div>
                                        : <div></div>
                                    }
                                </div>  
                                {this.props.loading 
                                    ? <button className="btn btn-primary mt-3 btn-lg" type="button" disabled>
                                         <span className="spinner-grow spinner-grow-sm" style={{'top' : '-1'}} role="status" aria-hidden="true"></span>
                                          &nbsp; Loading...
                                      </button>
                                     :  <button type="submit" className="btn btn-primary mt-3 btn-lg" data-toggle="modal">Proceed</button>
                                 }  
                                <p className="pt-4">Already have an account? <NavLink to='/signin' ><u>Log in here!</u></NavLink></p>    
                                <p className="alert alert-primary f-size2">
                                    <i className="fa fa-google fa-lg"></i> &nbsp;&nbsp;&nbsp;
                                    <i className="fa fa-mobile fa-lg"></i>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({signupError : state.authDetails.signupError, loading: state.authDetails.loading})
export default connect(mapStateToProps, {createAccount})(SignUp);
