import React, { Component } from 'react';
import PersonaType from './../personaComponent/personaType/type'; 
import { NavLink } from 'react-router-dom';
import {setPersonaScreen} from './../../services/actions/personaAction';
import { connect } from 'react-redux';
import { login, setUserDetails } from './../../services/actions/loginAction'

class SignIn extends Component {
    userData = {};
    handleSubmit = (event) => {
        event.preventDefault();
        this.props.login({email: this.props.userId, password: this.props.password});
        //this.props.setPersonaScreen('personaType');
        //$("#loadPersonaProfile").appendTo("body").modal("show"); 
        
    }
    handleChange = (event) => {
        const userData = {};
        if(event.target.name === 'email') {
            userData.userId = event.target.value;
            userData.password = this.props.password;
        }  else {
            userData.password = event.target.value;
            userData.userId = this.props.userId;
        }
        this.props.setUserDetails(userData);
    }
    render() {
        return (
            <div className="row container-height">
              <div className="home-background"></div>
              <div className="content">
                <div className="content-text ">
                    <div className="col-xs-12 col-sm-8 ml-auto mr-auto">
                        <h3 className="pb-3">Log in to account</h3>
                        <form noValidate onSubmit={this.handleSubmit}>
                            <div className="text-left form-group">
                                <label>Email address</label>
                                <input type="email" className={`form-control ${this.props.loginError && this.props.loginError.email ? "is-invalid" : ""}`} id="email_id" aria-describedby="emailHelp" name="email" placeholder="Enter email" onChange={this.handleChange} value={this.props.userId}/>
                                {this.props.loginError && this.props.loginError.email
                                ? <div className="invalid-feedback">{this.props.loginError.email}</div>
                                : <div></div>
                                }
                            </div>
                            <div className="text-left form-group">
                                <label>Password</label>
                                <input type="password" className={`form-control ${this.props.loginError && this.props.loginError.password ? "is-invalid" : ""}`} id="password" placeholder="Password" name="password" onChange={this.handleChange} value={this.props.password}/>
                                {this.props.loginError && this.props.loginError.password
                                    ? <div className="invalid-feedback">{this.props.loginError.password}</div>
                                    : <div></div>
                                }
                            </div>
                            {this.props.loading 
                               ? <button className="btn btn-primary mt-3 btn-lg" type="button" disabled>
                                    <span className="spinner-grow spinner-grow-sm" style={{'top' : '-1'}} role="status" aria-hidden="true"></span>
                                     &nbsp; Loading...
                                 </button>
                                : <button type="submit" className="btn btn-primary mt-3 btn-lg">Proceed</button>
                            }  
                            <p className="pt-4"> Don't have an account? <NavLink to='/signup' ><u>Sign up for free!</u></NavLink></p>
                        </form>
                    </div>
                    <div className="modal fade" id="loadPersonaProfile" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <PersonaType personaScreen = 'personaType'/>
                    </div>
                </div> 
              </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => (
    {
        loginError:   state.authDetails.loginError, 
        userId :state.authDetails.userId, 
        password: state.authDetails.password, 
        loading: state.authDetails.loading
    });
export default connect(mapStateToProps, {login, setPersonaScreen, setUserDetails})(SignIn);

