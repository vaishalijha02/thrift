import React, { Component } from 'react';
import './home.css'
import { NavLink } from 'react-router-dom';

class Home extends Component {
    stopVideo= () => {
        const myScope = document.getElementById('embed-video');      
        const iframes = myScope.getElementsByTagName("iframe");
            if (iframes != null) {
                for (let i = 0; i < iframes.length; i++) {
                    iframes[i].src = iframes[0].src;
            }
        }
    }
    render() {
        return (
                <div className="row container-height">
                    <div className="home-background"></div>
                    <div className="content pt-10">
                        <div className="content-text ">
                            <h1 className="container-header-text"> Know your credit score</h1>
                            <p> We are there to help!</p>
                            <p className="pt-3"><NavLink to="/login" className="bg-primary rounded-link " >Get Started</NavLink></p>  
                            <button className="btn" data-toggle="modal" data-target="#playVideo"><span className="white-text text-uppercase small"><u>See how it works</u></span></button>
                        </div>
                    </div>
                    <div className="modal fade" id="playVideo"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div className="modal-content p-0">
                                <div className="modal-body p-0">
                                <div className="embed-responsive embed-responsive-16by9" id="embed-video">
                                    <iframe title= "CIBIL Rank"className="embed-responsive-item" width="800" height="315" src="https://www.youtube.com/embed/vbKyBFGZACs" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe></div>
                               </div>
                                <div className="modal-footer ml-auto mr-auto">
                                 <button type="button" className="btn btn-outline-primary btn-lg" data-dismiss="modal" onClick={this.stopVideo} >Close</button>      
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
        )
    }
}
export default Home;