import React, { Component } from 'react';
import {setAccountType} from '../../../services/actions/personaAction';
import { connect } from 'react-redux';
import {NavLink } from 'react-router-dom';

class PersonaType extends Component {
  render() {
    return (  
      <div className="row container-height">
        <div className="home-background"></div>
        <div className="content">
          <div className="content-text">
            <h4 className="modal-title mb-3 text-center">Tell us about yourself!</h4>
            <form>
              <div className="form-group">
                <label>Account Type</label>
                <div className="form-group">
                  <select className="form-control" onChange={(e) => (this.props.setAccountType(e.target.value))}>
                    <option value="salariedProfessional">Salaried Professional</option>
                    <option value="freelancer">Freelancers / Business Person</option>
                  </select>
                </div>
              </div>
              {this.props.accountType === 'salariedProfessional' || this.props.accountType === undefined
              ? <div>
                  <div className="form-group">
                    <label>Is your savings account same as your salaried account?</label>
                    <div className="form-group">
                      <select className="form-control">
                        <option value="">Select</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                    </div>
                  </div>
                  <div className="form-group">
                    <label>How many accounts do you have?</label>
                    <div className="form-group">
                      <input type="number" className="form-control" />
                    </div>
                  </div>
                </div>
                : <div>
                    <div className="form-group">
                      <label>How many accounts do you have?</label>
                      <div className="form-group">
                        <input type="number" className="form-control" />
                      </div>
                    </div>
                  </div>
                }
                <NavLink className="nav-link" to="/persona/expense" ><span className="btn btn-primary btn-sm">Save</span></NavLink>
            </form>
          </div>
        </div>    
      </div>
    )
  }
}
const mapStateToProps = (state) => ({accountType: state.personaType.accountType })
export default connect(mapStateToProps, {setAccountType})(PersonaType);
