import React, { Component } from 'react';
import { connect } from 'react-redux';
import { manageMonthlyExpense, setSavingStrategy } from './../../../services/actions/personaAction';
import {NavLink } from 'react-router-dom';
class Expense extends Component {
    render() {
        return(
            <div className="row container-height">
                <div className="home-background"></div>
                <div className="content">
                    <div className="content-text">
                        <h4 className="modal-title mb-3 text-center">Tell us about yourself!</h4>
                        <form>
                            <div className="form-group text-left">
                                <label >Tell us how do you want to manage your monthly expenses? </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <input  type="radio" name="monthlyExpense" value="fixed" onClick={(e) => this.props.manageMonthlyExpense(e.target.value)}/>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control " value=" I want to set goals on my expenses in the start of the month" aria-label="Text input with radio button" readOnly/>
                                </div>
                                { this.props.monthlyExpenseMode === 'fixed'
                                    ? <div className="pl-2 small mt-2 bg-success text-white">Thrift will help you to manage your expenses. We will help you not overspent it</div>
                                    :<span></span>
                                }
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <input  type="radio" name="monthlyExpense"  value="indefinite" onClick={(e) => this.props.manageMonthlyExpense(e.target.value)}/>
                                    </div>
                                </div>
                                <input type="text" class="form-control " value="  I want to add expenses as I incur them" aria-label="Text input with radio button" readOnly/>
                            </div>
                            <div className="form-group text-left mt-3">
                                <label>Do you follow a savings strategy? </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <input  type="radio" name="followSavingStrategy"  value="yes" onChange={(e) => this.props.setSavingStrategy(e.target.value)}/>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control " value="  Yes" aria-label="Text input with radio button" readOnly/>
                                </div>
                                { this.props.followSavingStrategy === 'yes'
                                    ? <div className="pl-2 small mt-2 bg-success text-white">Our Smart suggestions should help you optimise your savings</div>
                                    :<span></span>
                                }
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <input  type="radio" name="followSavingStrategy" value="no" onChange={(e) => this.props.setSavingStrategy(e.target.value)}/>
                                    </div>
                                </div>
                                <input type="text" class="form-control " value="No" aria-label="Text input with radio button" readOnly/>
                            </div>
                            <NavLink className="nav-link" to="/dashboard" ><span className="btn btn-primary btn-lg">Save</span></NavLink>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({monthlyExpenseMode : state.personaType.monthlyExpenseMode, followSavingStrategy:  state.personaType.followSavingStrategy})
export default connect(mapStateToProps, {manageMonthlyExpense, setSavingStrategy})(Expense);