import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import './App.css';
import Route from './router';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <div className="container-fluid pl-0 pr-0">
            <Route />
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;
