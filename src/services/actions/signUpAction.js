import { SIGNUP_SUCCESS, SIGNUP_ERROR } from '../actions/actionTypes';
import axios from 'axios';
import browserHistory  from './../../utils/history';
import {setSpinner} from './loginAction';

export  function createAccount(userDetails) {
    
    return async (dispatch) => {
        dispatch(setSpinner(true));
        try {
            const response = await axios.post('/signup', userDetails);
            window.localStorage.setItem('authToken', response.data.token);
            alert('User Created Successfully');
            browserHistory.push('/persona/type');
            dispatch(setSpinner(false));
            return dispatch({
                type:  SIGNUP_SUCCESS,
                payload: {
                    authToken: response.data.token,
                    isAuthUser: true
                }
            })
        } catch (error) {
            dispatch(setSpinner(false));
            return dispatch({
                type:  SIGNUP_ERROR,
                payload: {
                    signupError: error.response ? error.response.data : 'something went wrong!'
                }
            })
        }
    }
}


