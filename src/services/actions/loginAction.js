import { LOGIN_SUCCESS, LOGIN_ERROR, USER_DETAILS, LOG_OUT, SET_SPINNER} from '../actions/actionTypes';
import axios from 'axios';
import browserHistory  from './../../utils/history';

export  function login(userDetails) {
    
    return async (dispatch) => {
        dispatch(setSpinner(true));
        try {
            const response = await axios.post('/login', userDetails);
            window.localStorage.setItem('authToken', response.data.token);
            alert('User Logged In Successfully');
            this.setUserDetails({});
            browserHistory.push('/persona/type');
            dispatch(setSpinner(false));
            return dispatch({
                type:  LOGIN_SUCCESS,
                payload: {
                    authToken: response.data.token,
                    isAuthUser: true
                }
            })
        } catch (error) {
            this.setUserDetails({});
            dispatch(setSpinner(false));
            return dispatch({
                type:  LOGIN_ERROR,
                payload: {
                    loginError: error.response ? error.response.data : 'something went wrong!'
                }
            })
        }
    }
}
export  function setUserDetails(userDetails) {
    return async (dispatch) => {
        return dispatch({
            type:  USER_DETAILS,
            payload: {
                userId : userDetails.userId ? userDetails.userId: '',
                password : userDetails.password ? userDetails.password: ''
            }
        })
    }
}
export function logOut() {
    return async (dispatch) => {
        window.localStorage.setItem('authToken', '');
        return dispatch({
            type:  LOG_OUT,
            payload: {
                authToken : '',
                isAuthUser: false
            }
        })
    }
}

export function setSpinner(isLoading) {
    return async (dispatch) => {
        return dispatch({
            type:  SET_SPINNER,
            payload: {
                loading : isLoading
            }
        })
    }
}


