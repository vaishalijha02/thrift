import { ACCOUNT_TYPE, PERSONA_SCREEN, MONTHLY_EXPENSE, SAVING_STRATEGY } from './actionTypes';

export function setAccountType(accountType) {
    return function(dispatch) {
        return dispatch({
            type:  ACCOUNT_TYPE,
            payload: {
                accountType: accountType
            }
        })
    }
}
export function setPersonaScreen(personaScreen) {
    return function(dispatch) {
        return dispatch({
            type:  PERSONA_SCREEN,
            payload: {
                personaScreen: personaScreen
            }
        })
    }
}

export function manageMonthlyExpense(monthlyExpenseMode) {
    return function(dispatch) {
        return dispatch({
            type:  MONTHLY_EXPENSE,
            payload: {
                monthlyExpenseMode: monthlyExpenseMode
            }
        })
    }
}

export function setSavingStrategy(followSavingStrategy) {
    return function(dispatch) {
        return dispatch({
            type:  SAVING_STRATEGY,
            payload: {
                followSavingStrategy: followSavingStrategy
            }
        })
    }
}