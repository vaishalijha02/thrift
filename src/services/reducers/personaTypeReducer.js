import { ACCOUNT_TYPE, PERSONA_SCREEN, MONTHLY_EXPENSE, SAVING_STRATEGY } from '../actions/actionTypes';

const initialState = { accountType : 'salariedProfessional' , personaScreen: '', monthlyExpenseMode: '', followSavingStrategy: ''}
export default function BasketReducer(state = initialState, action) {
    switch(action.type) {
            case ACCOUNT_TYPE:
                return {...state, accountType: action.payload.accountType};
            case PERSONA_SCREEN:
                return {...state, personaScreen: action.payload.personaScreen};
            case MONTHLY_EXPENSE:
                return {...state, monthlyExpenseMode: action.payload.monthlyExpenseMode};
            case SAVING_STRATEGY:
                return {...state, followSavingStrategy: action.payload.followSavingStrategy};
             default:
                return state;
    }
}