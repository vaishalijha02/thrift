import { LOGIN_SUCCESS, LOGIN_ERROR , USER_DETAILS, SIGNUP_SUCCESS, SIGNUP_ERROR, LOG_OUT, SET_SPINNER } from '../actions/actionTypes';

const initialState = { 
    authToken : '', 
    loginError : null,
    userId : '' , 
    password: '', 
    isAuthUser: false, 
    signupError: null,
    loading: false
}
export default function logInReducer(state = initialState, action) {
    switch(action.type) {
        case LOGIN_SUCCESS:
            return {...state, authToken: action.payload.authToken, loginError: null, isAuthUser: action.payload.isAuthUser};
        case LOGIN_ERROR:
            return {...state, loginError: action.payload.loginError};
            case LOG_OUT:
            return {...state, authToken: action.payload.authToken,  isAuthUser: action.payload.isAuthUser};
        case USER_DETAILS:
            return {...state, userId: action.payload.userId, password: action.payload.password};
        case SIGNUP_SUCCESS:
            return {...state, authToken: action.payload.authToken, signupError: null, isAuthUser: action.payload.isAuthUser};
        case SIGNUP_ERROR:
            return {...state, signupError: action.payload.signupError};
        case SET_SPINNER: 
            return {...state, loading: action.payload.loading};
        default:
            return state;
    }
}