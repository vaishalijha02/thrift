import { combineReducers } from 'redux';
import PersonaType from './services/reducers/personaTypeReducer';
import AuthReducer from './services/reducers/authReducer';


export default combineReducers({
    personaType: PersonaType,
    authDetails: AuthReducer

});